
- Do you prefer Swift or Objective-C? Why?

- What is the difference between frame and bound ?

- What the keyword final stands for?

- What the keyword static stands for?

- What is class function?

- What is the difference between instance and class functions?

- What is the difference between class and static functions?

- What is the most important UI element in iOS for you? Why? (Expected answer: UITableView ; no “right” answer)

- What is reuseIdentifier?

- How many states an application can be in? Could you name them, and explain the difference between them? (5 States: Not-running, Inactive, Active, Background, Suspended)

- Why would you use extensions in swift?

- What is ARC/MRC?

- What is the difference between strong and weak?

#Code Snippet with non-weak delegate ~ Find and correct the mistake.

- What is retain-cycle? How can we escape from it?

#Code Snippet with retain-cycle ~ Find and correct the mistake.
# class A { var propertyB: B }
# class B { var propertyA: A } -> class B { weak var propertyA: A }

- When an object in Swift is being deinitialized? (Reference count = 0)

- Can you explain why somebody would prefer creating UI using XIB/Storyboard/programatically?

- What is the difference between viewDidLoad and viewDidAppear? Where would you execute repeatable UI refresh functionality?

- How many ways do you know for storing data on iOS device? What is the difference between them? (UserDefault, KeyChains; CoreData)

- Where would you persist high score of a game? Why? (No right answer)

- How can you check if this app has been previously installed on this device? ( Keychain )

- What is App Groups? How can you transfer data between apps in one App Group? (Shared-UserDefaults/Keychains)

- What are App Extensions? Can you name some? (Today Widget/ Spotlight/ CallDirectory/ etc.) - How can you transfer data between the main App and App extension -> App Groups.

- What is closure?

- What is the difference between Stack and Heap? (What is Stack Overflow? What memory is stored in the Heap? What are the sizes of the two?)

- What is the difference between viewDidAppear and viewWillAppear?

- What is the difference between var and let?

- Explain what is rawValue?

#enum ExampleEnum: String {
#	case caseOne = “1”
#	case caseTwo
#	case caseThree
#}
#What is the rawValue of ExampleEnum.caseTwo.rawValue

- What is overloading?

- What is overriding?

- What is lazy?

- Could you explain the difference between computed and stored variables?

- What is the difference between set and didSet? (Why would you use either one?)

- What can you say about filter/map/flatMap/reduce functions? (What is zip?)

- What is Codable?

- What is CocoaPods? (Carthage/Homebrew)

- Can you list some ad-hoc distribution methods for iOS apps? (TestFlight; Fastlane; Fabric, etc…)

- How to create copy of a custom object? (NSCopying, copyWithZone(:)) #Why do we need to do this?

- What is the difference between weak and unowned?

- How would you implement tableView that needs to download images from internet? (What if the images are with different aspect ratio?/ What if the images are too big? What if the images are constantly the same - never change)

- Do you have experience working with …. ?(Development Process related tools & methodologies: Gerrit/Swiftlint/Codereviews/Pull requests/Git Flow/Scrum(Poker planning, WBS, estimates)/SourceTree/Jira)

- Can you say some specifics about Exceptions in iOS? (Define custom Error; throw-catch; try?)

- What is discardableResult?

- What is the difference between escaping and non-escaping closures?

- What do you think about XCTests/TDD? (No right answer)

- What is force unwrapping? (NB* important - ask additional question to understand how much the person knows about this - optional chaining; optional binding; if let; guard let; nil coalescing )

- What is the difference between if let and guard let? (Additional knowledge: guard let needs to stop the flow: break/return)

- What are size classes?

- Can you give example of a subscript? Have you written custom subscripts?

- What is the difference between Any and AnyObject?

- How can you pass data between ViewControllers in iOS?

- Design Patterns question: (MVC / Observer / Delegate / Target-Action / Singleton are must; gang of four for additional info: State, Factory, Decorator, Mediator, Facade, Builder … are nice to know and other… )

- Can you give examples from iOS related to the above DPs?

- Are you familiar with MVVM/VIPER? Have you been working on iOS application based on the that architecture? What do you think about it? (No right answer)















